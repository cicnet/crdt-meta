var webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    index: './dist/cjs/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist-web'),
    filename: 'crdt-meta.web.js',
    library: 'crdtMeta',
    libraryTarget: 'window'
  },
  mode: 'development',
  performance: {
    hints: false
  },
  stats: 'errors-only',
  resolve: {
	  fallback: {
        "crypto": false,
        "path": false,
        "fs": false,
        "os": false,
        "child_process": false,
      }
  },
};
